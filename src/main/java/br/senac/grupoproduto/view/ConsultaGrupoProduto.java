package br.senac.grupoproduto.view;

import br.senac.grupoproduto.model.GrupoProdutoDAO;
import br.senac.produto.model.GrupoProduto;
import javax.swing.table.DefaultTableModel;

public class ConsultaGrupoProduto extends javax.swing.JFrame {
    public ConsultaGrupoProduto() {
        initComponents();
        buscar();
    }
    public void buscar(){
        DefaultTableModel model = (DefaultTableModel) tabela.getModel();
        model.setRowCount(0);
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        for (GrupoProduto gp : dao.listarPorNome(campoBusca.getText())){
            Object[] linha = new Object[3];
            linha[0] = gp.getIdGrupoProduto();
            linha[1] = gp.getNomeGrupoProduto();
            linha[2] = gp.getTipoProduto().getNome();
            model.addRow(linha);
        }
    }  
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultaGrupoProduto().setVisible(true);
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        menuPopUp = new javax.swing.JPopupMenu();
        novoPopUp = new javax.swing.JMenuItem();
        alterarPopUp = new javax.swing.JMenuItem();
        excluirPopUp = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        labelNome = new javax.swing.JLabel();
        campoBusca = new javax.swing.JTextField();
        buscar = new javax.swing.JButton();
        jToolBar2 = new javax.swing.JToolBar();
        novo = new javax.swing.JButton();
        alterar = new javax.swing.JButton();
        excluir = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        fechar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        menu = new javax.swing.JMenuBar();
        menuTarefas = new javax.swing.JMenu();
        manuNovo = new javax.swing.JMenuItem();
        manunAlterar = new javax.swing.JCheckBoxMenuItem();
        menuExcluir = new javax.swing.JCheckBoxMenuItem();
        menuSobre = new javax.swing.JMenu();

        jToolBar1.setRollover(true);

        jLabel1.setText("Nome:");
        jToolBar1.add(jLabel1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jPanel2.setLayout(new java.awt.BorderLayout());

        menuPopUp.setComponentPopupMenu(menuPopUp);

        novoPopUp.setText("Novo");
        novoPopUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoPopUpActionPerformed(evt);
            }
        });
        menuPopUp.add(novoPopUp);

        alterarPopUp.setText("Alterar");
        menuPopUp.add(alterarPopUp);

        excluirPopUp.setText("Excluir");
        menuPopUp.add(excluirPopUp);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setLayout(new java.awt.BorderLayout());

        labelNome.setText("Nome:");

        campoBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                campoBuscaActionPerformed(evt);
            }
        });

        buscar.setText("Buscar");
        buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarActionPerformed(evt);
            }
        });

        jToolBar2.setRollover(true);

        novo.setText("Novo");
        novo.setFocusable(false);
        novo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        novo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoActionPerformed(evt);
            }
        });
        jToolBar2.add(novo);

        alterar.setText("Alterar");
        alterar.setFocusable(false);
        alterar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        alterar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar2.add(alterar);

        excluir.setText("Excluir");
        excluir.setFocusable(false);
        excluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        excluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar2.add(excluir);
        jToolBar2.add(filler1);

        fechar.setText("Fechar");
        fechar.setFocusable(false);
        fechar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        fechar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        fechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fecharActionPerformed(evt);
            }
        });
        jToolBar2.add(fechar);

        jScrollPane1.setMaximumSize(new java.awt.Dimension(32747, 32767));

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Código", "Descricão", "Tipo do Produto"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabela.setComponentPopupMenu(menuPopUp);
        jScrollPane1.setViewportView(tabela);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(labelNome)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(campoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buscar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(campoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel4, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        menuTarefas.setText("Tarefas");

        manuNovo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        manuNovo.setMnemonic('N');
        manuNovo.setText("Novo");
        manuNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manuNovoActionPerformed(evt);
            }
        });
        menuTarefas.add(manuNovo);

        manunAlterar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        manunAlterar.setMnemonic('U');
        manunAlterar.setSelected(true);
        manunAlterar.setText("Alterar");
        menuTarefas.add(manunAlterar);

        menuExcluir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        menuExcluir.setMnemonic('E');
        menuExcluir.setSelected(true);
        menuExcluir.setText("Excluir");
        menuExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExcluirActionPerformed(evt);
            }
        });
        menuTarefas.add(menuExcluir);

        menu.add(menuTarefas);

        menuSobre.setText("Sobre");
        menu.add(menuSobre);

        setJMenuBar(menu);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscarActionPerformed

    private void campoBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_campoBuscaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_campoBuscaActionPerformed

    private void novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_novoActionPerformed

    private void manuNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manuNovoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_manuNovoActionPerformed

    private void menuExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExcluirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuExcluirActionPerformed

    private void fecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fecharActionPerformed
        dispose();
    }//GEN-LAST:event_fecharActionPerformed

    private void novoPopUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoPopUpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_novoPopUpActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton alterar;
    private javax.swing.JMenuItem alterarPopUp;
    private javax.swing.JButton buscar;
    private javax.swing.JTextField campoBusca;
    private javax.swing.JButton excluir;
    private javax.swing.JMenuItem excluirPopUp;
    private javax.swing.JButton fechar;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel labelNome;
    private javax.swing.JMenuItem manuNovo;
    private javax.swing.JCheckBoxMenuItem manunAlterar;
    private javax.swing.JMenuBar menu;
    private javax.swing.JCheckBoxMenuItem menuExcluir;
    private javax.swing.JPopupMenu menuPopUp;
    private javax.swing.JMenu menuSobre;
    private javax.swing.JMenu menuTarefas;
    private javax.swing.JButton novo;
    private javax.swing.JMenuItem novoPopUp;
    private javax.swing.JTable tabela;
    // End of variables declaration//GEN-END:variables
}
