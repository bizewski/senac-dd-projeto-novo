package br.senac.grupoproduto.model;

import br.senac.dd.model.BaseDAO;
import br.senac.componentes.db.ConexaoDB;
import br.senac.componentes.db.UtilSQL;
import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GrupoProdutoJDBCDAO implements BaseDAO<GrupoProduto, Integer> {
    public static void main(String[] args) throws SQLException {
        
        GrupoProdutoJDBCDAO dao = new GrupoProdutoJDBCDAO();
        GrupoProduto gp = new GrupoProduto();
        gp.setTipoProduto(TipoProduto.MERCADORIA);
        gp.setNomeGrupoProduto("teste");
        gp.setPercDesconto(1.);
        
        Integer id = dao.inserir(gp);
        
    }

    public GrupoProdutoJDBCDAO() {
        
    }

    public Integer inserir(GrupoProduto grupoProduto) throws SQLException {
       // if(grupoProduto.getDataInclusao() == null)
            grupoProduto.setDataInclusao(new Date());
        
        Integer pk = 0;
        String sqlInsert = "INSERT INTO GRUPOPRODUTO"
                + "(NOMEGRUPOPRODUTO, TIPO, DATAINCLUSAO, PERCDESCONTO)"
                + "VALUES( ";
        sqlInsert += "'" + grupoProduto.getNomeGrupoProduto() + "', ";

        if (grupoProduto.getTipoProduto() == null) {
            throw new RuntimeException("Tipo grupo não pode ser nulo");
        } else {
            sqlInsert += grupoProduto.getTipoProduto().getId() + ",";
            sqlInsert += UtilSQL.getDataTempoToSQL(grupoProduto.getDataInclusao()) + ",";
            sqlInsert += grupoProduto.getPercDesconto() + ")";
            System.out.println(sqlInsert);
        }
        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sqlInsert += "{ts '" + sdf.format(grupoProduto.getDataInclusao()) + "'}, ";
        sqlInsert += grupoProduto.getPercDesconto() + ")";
        System.out.println(sqlInsert);
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regCriados = stm.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);
        ResultSet rsPK = stm.getGeneratedKeys(); // obter PK gerada
        if (rsPK.next()) {
            pk = rsPK.getInt(1);
            return pk;
        }
           

        throw  new RuntimeException("Erro inesperado ao incluir grupo produto");

    }

    public boolean alterar(GrupoProduto grupoProduto) {
        String sqlAlterar = "UPDATE GRUPOPRODUTO SET"
                + " NOMEGRUPOPRODUTO = '"+ grupoProduto.getNomeGrupoProduto() +"', \n"
                + "tipo = 1,\n"
                +"percDesconto = 1.2 \n"
                + " where idgrupoproduto = " + grupoProduto.getIdGrupoProduto();
        
        return false;
    }

    public boolean excluir(Integer id) {
        return false;  
    }    

    public GrupoProduto getPorId(Integer id) throws SQLException {
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        String sql = "Select + from grupoproduto"
                + " where idgrupoproduto" + id;
        ResultSet rs = stm.executeQuery(sql);
        if (rs.next() == false) {
            throw new RuntimeException("Registro não encontrado!");
        }
        GrupoProduto gp = new GrupoProduto();
        gp.setIdGrupoProduto(rs.getInt("nomeGrupoProduto"));

        return gp;

    }

    /**
     * Faça um foreach na lista e para os grupos de contiverem apenas parte da
     * string do nome passada como parâmetro, adicione o mesmo em outra lista
     * que será retornada para o usuário. Esse método deverá ser case
     * insensitive; método contains serve para ver se dentro de uma string tem
     * determinado conjunjto de caracteres.
     *
     * @param nome
     * @return
     */
    public List<GrupoProduto> listarGrupoProdutoPorNome(String nome) {
        if (nome == null) {
            return listarTodos();
        }
        List<GrupoProduto> lista = new ArrayList<>();
        //implemente aqui
        return lista;
    }

    /**
     * se for para ordenar de forma crescente, utilize Comparator; se for para
     * ordenar de forma descrente, utilize Comparable.
     *
     * @param crescente
     * @param lista
     * @return
     */
    public List<GrupoProduto> ordenarPorNome(boolean crescente) {
        List<GrupoProduto> lista = listarTodos();
        //Implemente aqui
        return lista;
    }

    /**
     * converta o ArrayList para um LinkedList retorne a lista ordenada por
     * idGrupoProduto, utilize Comparator e Collections.sort;
     *
     * @return
     */
    public List<GrupoProduto> listarTodos() {
        LinkedList<GrupoProduto> listaTodos = new LinkedList<>();
        //Implemente aqui
        return listaTodos;
    }

    /**
     * Use um HashMap e separe a lista de Grupos de Produtos em duas, uma
     * contendo apenas o TipoProduto.PRODUTO e outra TipoProduto.SERVICO. Depois
     * imprimiva os chaves e na sequência imprima os valores.
     *
     * @return
     */
    public HashMap<TipoProduto, List<GrupoProduto>> obterMapId() {
        HashMap<TipoProduto, List<GrupoProduto>> hashMap = new HashMap<>();
        hashMap.put(TipoProduto.SERVICO, new ArrayList<>());
        hashMap.put(TipoProduto.MATERIA_PRIMA, new ArrayList<>());
        hashMap.put(TipoProduto.MERCADORIA, new ArrayList<>());

        //Implemente aqui
        for (List<GrupoProduto> listaGrupProd : hashMap.values()) {
            for (GrupoProduto grupProd : listaGrupProd) {
                System.out.println(grupProd);
            }
        }

        for (TipoProduto tipo : hashMap.keySet()) {
            System.out.println("Tipo: " + tipo);
        }

        return hashMap;
    }

    @Override
    public long inserir(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}