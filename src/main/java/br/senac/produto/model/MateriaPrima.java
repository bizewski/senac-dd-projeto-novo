package br.senac.produto.model;
public class MateriaPrima extends Produto {
    private Long idMateriaPrima;
    private Float percIPI;
    
    public MateriaPrima(Long idMateriaPrima, Float percIPI) {
        this.idMateriaPrima = idMateriaPrima;
        this.percIPI = percIPI;
    }

    public MateriaPrima(Long idMateriaPrima, Float percIPI, Long idProduto) {
        super(idProduto);
        this.idMateriaPrima = idMateriaPrima;
        this.percIPI = percIPI;
    }

    public Long getIdMateriaPrima() {
        return idMateriaPrima;
    }

    public void setIdMateriaPrima(Long idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    public Float getPercIPI() {
        return percIPI;
    }

    public void setPercIPI(Float percIPI) {
        this.percIPI = percIPI;
    }

    @Override
    public Float getTotalPerImposto() {
        getPercIPI();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
