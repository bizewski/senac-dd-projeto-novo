package br.senac.produto.model;
public class Servico extends Produto {
    private Long idServico;
    private Float percISS;
    
    //public Float getTotalPercImposto();

    public Servico(Long idServico, Float percISS) {
        this.idServico = idServico;
        this.percISS = percISS;
    }

    public Servico(Long idServico, Float percISS, Long idProduto) {
        super(idProduto);
        this.idServico = idServico;
        this.percISS = percISS;
    }

    public Long getIdServico() {
        return idServico;
    }

    public void setIdServico(Long idServico) {
        this.idServico = idServico;
    }

    public Float getPercISS() {
        return percISS;
    }

    public void setPercISS(Float percISS) {
        this.percISS = percISS;
    }

    @Override
    public Float getTotalPerImposto() {
        getPercISS();
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
