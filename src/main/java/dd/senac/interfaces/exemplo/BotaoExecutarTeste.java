package dd.senac.interfaces.exemplo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
public class BotaoExecutarTeste implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
    JOptionPane.showMessageDialog(null, "Interface ActionListener, que representa eventos de ação,"
        +"tal como o clique do botão, \n"
        +"foi implementada pela classe concreta BotaoExecutarTeste.");    }
}
